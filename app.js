const tls = require("node:tls")
const http = require("http")
const express = require("express")
const cors = require("cors")
const auth_router = require("./routes/auth")
const fs = require('fs')
const path = require('path')
const app = express()
const PORT = 1337
const HOST = '127.0.0.1'
const value = null;
app.use(cors())

app.use('/oauth',auth_router)

const options={
    cert:fs.readFileSync('keys/auth-server-key.key'),
    key:fs.readFileSync('keys/auth-server-cert.key'),
    rejectUnauthorized: true,
}

const server = tls.createServer(options,app);
    server.listen(PORT,HOST, () => {
    console.log('server bound');
  })












