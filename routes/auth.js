const express = require("express")
const router = express.Router()
const jwt = require('../jwt')

router.post('/token',(req,res)=>{
 let payload= {name:"name",email:"xxx@gmail.com",expiresIn:'30d'}
 jwt.signToken(payload,(err,response)=>{
    if(err){
        res.status(400).json({error:err,code:err.code||500})
    }else{
     res.status(200).json({code:"200 ok",access_token:response})
    }
 })
})

module.exports =router