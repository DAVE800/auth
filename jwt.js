const fs = require('fs')
const path = require('path')
const jwt = require('jsonwebtoken')
const privateKey = fs.readFileSync(path.join('keys', 'auth-server-cert.key'), 'utf8')
const publicKey = fs.readFileSync(path.join('keys', 'auth-server-key.key'), 'utf8')
const bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports = {

    signToken: async(payload,callback) => {
        try {
            let token = await jwt.sign(payload, privateKey, { algorithm: 'RS256'})
            return callback(null,token);
        } catch (err) {
            return callback({error:err,code:400},null)
        }
    },

    verifyToken: async(token,callback) => {
        try {
           let verified=await jwt.verify(token, publicKey, { algorithm: 'RS256'})
            return callback(null,verified);
        } catch (err) {
          return callback({error:err,code:401},null)
        }
    },


  
    hashPassword:async(password,callback)=>{
      await  bcrypt.hash(password, saltRounds, function(err, hash) {
            return callback(hash)
        });

    }


}
